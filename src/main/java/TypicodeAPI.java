import Objects.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class TypicodeAPI {

    private final String app = "https://jsonplaceholder.typicode.com";
    private String posts = "/posts";
    private String comments = "/comments";
    private String albums = "/albums";
    private String photos = "/photos";
    private String todos = "/todos";
    private String users = "/users";

    public List<Post> getPosts() throws IOException {
        String response = HTTPUtils.doGET(this.app + posts);
        return new ObjectMapper().readValue(response, new TypeReference<List<Post>>(){});
    }

    public List<Comment> getComments() throws IOException {
        String response = HTTPUtils.doGET(this.app + comments);
        return new ObjectMapper().readValue(response, new TypeReference<List<Comment>>(){});
    }

    public List<Album> getAlbums() throws IOException {
        String response = HTTPUtils.doGET(this.app + albums);
        return new ObjectMapper().readValue(response, new TypeReference<List<Album>>(){});
    }

    public List<Photo> getPhotos() throws IOException {
        String response = HTTPUtils.doGET(this.app + photos);
        return new ObjectMapper().readValue(response, new TypeReference<List<Photo>>(){});
    }

    public List<Todo> getToDos() throws IOException {
        String response = HTTPUtils.doGET(this.app + todos);
        return new ObjectMapper().readValue(response, new TypeReference<List<Todo>>(){});
    }

    public List<User> getUsers() throws IOException {
        String response = HTTPUtils.doGET(this.app + users);
        return new ObjectMapper().readValue(response, new TypeReference<List<User>>(){});
    }


}
