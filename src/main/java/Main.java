import Objects.*;

import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        TypicodeAPI api = new TypicodeAPI();

        List<Post> posts = api.getPosts();
        List<Comment> comments = api.getComments();
        List<Album> albums= api.getAlbums();
        List<Photo> photos = api.getPhotos();
        List<Todo> todos = api.getToDos();
        List<User> users = api.getUsers();

        for (User user : users){
            System.out.println(user.getAddress().getGeoLocation().getLatitude() + " : " + user.getAddress().getGeoLocation().getLongitude());
        }
    }
}