package Objects;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class Comment {

    private int postId;
    private int id;
    private String name;
    private String email;
    private String body;

    @JsonGetter
    public int getPostId() { return this.postId; }
    @JsonGetter
    public int getId() { return this.id; }
    @JsonGetter
    public String getName() { return this.name; }
    @JsonGetter
    public String getEmail() { return this.email; }
    @JsonGetter
    public String getBody() { return this.body; }

    @JsonSetter
    public void setPostId(int postId) { this.postId = postId; }
    @JsonSetter
    public void setId(int id) { this.id = id; }
    @JsonSetter
    public void setName(String name) { this.name = name; }
    @JsonSetter
    public void setEmail(String email) { this.email = email; }
    @JsonSetter
    public void setBody(String body) { this.body = body; }

}
