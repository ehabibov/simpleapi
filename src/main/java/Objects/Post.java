package Objects;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class Post {

    private int userId;
    private int id;
    private String title;
    private String body;

    @JsonGetter
    public int getUserId() { return this.userId; }
    @JsonGetter
    public int getId() { return this.id; }
    @JsonGetter
    public String getTitle() { return this.title; }
    @JsonGetter
    public String getBody() { return this.body;}


    @JsonSetter
    public void setUserId(int userId) { this.userId = userId; }
    @JsonSetter
    public void setId(int id) { this.id = id; }
    @JsonSetter
    public void setTitle(String title) { this.title = title; }
    @JsonSetter
    public void setBody(String body) { this.body = body; }

}
