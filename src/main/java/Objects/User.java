package Objects;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class User {

    private int id;
    private String name;
    private String username;
    private String email;
    private String phone;
    private String website;
    private Address address;
    private Company company;

    @JsonGetter
    public int getId() { return this.id; }
    @JsonGetter
    public String getName() { return this.name; }
    @JsonGetter
    public String getUsername() { return this.username; }
    @JsonGetter
    public String getEmail() { return this.email; }
    @JsonGetter
    public String getPhone() { return this.phone; }
    @JsonGetter
    public String getWebsite() { return this.website; }
    @JsonGetter
    public Address getAddress() { return this.address; }
    @JsonGetter
    public Company getCompany() { return this.company; }

    @JsonSetter
    public void setId(int id) { this.id = id; }
    @JsonSetter
    public void setName(String name) { this.name = name; }
    @JsonSetter
    public void setUsername(String username) { this.username = username; }
    @JsonSetter
    public void setEmail(String email) { this.email = email; }
    @JsonSetter
    public void setPhone(String phone) { this.phone = phone; }
    @JsonSetter
    public void setWebsite(String website) { this.website = website; }
    @JsonSetter
    public void setAddress(Address address) { this.address = address; }
    @JsonSetter
    public void setCompany(Company company) { this.company = company; }

}
