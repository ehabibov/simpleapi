package Objects;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class Todo {
    private int userId;
    private int id;
    private String title;
    private boolean completed;

    @JsonGetter
    public int getUserId() { return this.userId; }
    @JsonGetter
    public int getId() { return this.id; }
    @JsonGetter
    public boolean isCompleted() { return this.completed; }
    @JsonGetter
    public String getTitle() { return this.title; }
    @JsonSetter
    public void setUserId(int userId) { this.userId = userId; }
    @JsonSetter
    public void setId(int id) { this.id = id; }
    @JsonSetter
    public void setTitle(String title) { this.title = title; }
    @JsonSetter
    public void setCompleted(boolean completed) { this.completed = completed; }
}
