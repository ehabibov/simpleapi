package Objects;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class Photo {

    private int albumId;
    private int id;
    private String title;
    private String url;
    private String thumbnailUrl;

    @JsonGetter
    public int getAlbumId() { return this.albumId; }
    @JsonGetter
    public int getId() { return this.id; }
    @JsonGetter
    public String getTitle() { return this.title; }
    @JsonGetter
    public String getUrl() { return this.url; }
    @JsonGetter
    public String getThumbnailUrl() { return this.thumbnailUrl; }

    @JsonSetter
    public void setAlbumId(int albumId) { this.albumId = albumId; }
    @JsonSetter
    public void setId(int id) { this.id = id; }
    @JsonSetter
    public void setTitle(String title) { this.title = title; }
    @JsonSetter
    public void setUrl(String url) { this.url = url; }
    @JsonSetter
    public void setThumbnailUrl(String thumbnailUrl) { this.thumbnailUrl = thumbnailUrl; }

}
