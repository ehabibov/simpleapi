package Objects;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class GeoLocation {

    private double latitude;
    private double longitude;

    @JsonGetter("lat")
    public double getLatitude() { return this.latitude; }
    @JsonGetter("lng")
    public double getLongitude() { return this.longitude; }

    @JsonSetter("lat")
    public void setLatitude(double latitude) { this.latitude = latitude; }
    @JsonSetter("lng")
    public void setLongitude(double longitude) { this.longitude = longitude; }

}
