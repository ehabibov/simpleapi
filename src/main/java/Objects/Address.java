package Objects;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class Address {

    private String street;
    private String suite;
    private String city;
    private String zipcode;
    private GeoLocation geoLocation;

    @JsonGetter
    public String getStreet() { return this.street; }
    @JsonGetter
    public String getSuite() { return this.suite; }
    @JsonGetter
    public String getCity() { return this.city; }
    @JsonGetter
    public String getZipcode() { return this.zipcode; }
    @JsonGetter("geo")
    public GeoLocation getGeoLocation() { return this.geoLocation; }

    @JsonSetter
    public void setStreet(String street) { this.street = street; }
    @JsonSetter
    public void setSuite(String suite) { this.suite = suite; }
    @JsonSetter
    public void setCity(String city) { this.city = city; }
    @JsonSetter
    public void setZipcode(String zipcode) { this.zipcode = zipcode; }
    @JsonSetter("geo")
    public void setGeoLocation(GeoLocation geoLocation) { this.geoLocation = geoLocation; }



}
