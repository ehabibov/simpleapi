package Objects;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class Company {

    private String name;
    private String catchPhrase;
    private String businessService;

    @JsonGetter
    public String getName() { return this.name; }
    @JsonGetter
    public String getCatchPhrase() { return this.catchPhrase; }
    @JsonGetter("bs")
    public String getBusinessService() { return this.businessService; }

    @JsonSetter
    public void setName(String name) { this.name = name; }
    @JsonSetter
    public void setCatchPhrase(String catchPhrase) { this.catchPhrase = catchPhrase; }
    @JsonSetter("bs")
    public void setBusinessService(String businessService) { this.businessService = businessService; }

}
